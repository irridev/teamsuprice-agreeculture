import 'dart:async';
import 'package:flutter/material.dart';
import 'package:matchoff/models/user.dart';
// import 'package:matchoff/delegates/search.dart';
import 'package:matchoff/utils/db_helper.dart';
import 'package:matchoff/screens/user_detail.dart';
import 'package:matchoff/navs/app_drawer.dart';
import 'package:sqflite/sqflite.dart';

class Home extends StatefulWidget {
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  DatabaseHelper databaseHelper = DatabaseHelper();
  List<User> userList;
  int count = 0;
    
  @override
  Widget build(BuildContext context) {
    if (userList == null) {
      userList = List<User>();
      updateListView();
    }
    // if (this.userListheart == null) {}

    return Scaffold(
      appBar: AppBar(
        title: Text('Jobs & Job-seekers'),
        // actions: <Widget>[
        //   IconButton(icon: Icon(Icons.search), 
        //     onPressed: (){showSearch(context: context, delegate: SearchPost());}
        //   ),
        //   IconButton(
        //     icon: Icon(Icons.save_alt),
        //     onPressed: () => show_modal('To send data, please take a screenshot of the participants list and email them to soraplatform@gmail.com', 'We are still working on the data export feature'),
        //   ),
        // ],
      ),
      drawer: AppDrawer(),
      body: getUserListView(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.cyan[600],
        onPressed: () {
          debugPrint('FAB clicked');
          // Navigator.push(context, MaterialPageRoute(builder: (context) => UserDetail()),);
          // Navigator.push(context, MaterialPageRoute(builder: (context) {
          //   return UserDetail(User('', '', '', '', '', '', '', 2), 'Add User');
          // }));
          navigateToDetail(User('', '', '', '', '', '', '', '', 2), 'Add User');
        },
        tooltip: 'Add User',
        child: Icon(Icons.add),
      ),
    );
  }

  ListView getUserListView() {
    TextStyle titleStyle = Theme.of(context).textTheme.subhead;
    return ListView.builder(
      
      itemCount: count,
      itemBuilder: (BuildContext context, int position) {
        return Card(
          color: Colors.white,
          elevation: 2.0,
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: getGenderColor(this.userList[position].gender),
              child: getGenderIcon(this.userList[position].gender),
            ),
            title: new Row(
              children: <Widget>[
                Text(this.userList[position].name, style: TextStyle(fontSize: 16)),
              ],
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Text(this.userList[position].description),
                // Text(this.userList[position].heart.toStringAsFixed(2)),
                // Text(this.userList[position].mars.toStringAsFixed(2)),
                Text(this.userList[position].email),
                Text(this.userList[position].job),
                Text(this.userList[position].city),
                // Text("Heart: $heart"),
              // Text(this.userList[position].date, style: TextStyle(fontSize: 10))
              ],
            ),
            onTap: () {
              debugPrint("ListTile Tapped");  
              navigateToDetail(this.userList[position], 'Edit User');
              // Navigator.push(context, MaterialPageRoute(builder: (context) {
              //   return UserDetail(this.userList[position], 'Edit');
              // }));
            }
          ),
        );
      },
    );
  }

  // Returns the gender color
  Color getGenderColor(String gender) {
    switch (gender) {
      case 'male':
        return Colors.blue;
        break;
      case 'female':
        return Colors.pink;
        break;
      default:
        return Colors.blue;
    }
  }

  // Returns the priority icon
  Icon getGenderIcon(String gender) {
    switch (gender) {
      case 'male':
        return Icon(Icons.face);
        break;
      case 'female':
        return Icon(Icons.pregnant_woman);
        break;
      default:
        return Icon(Icons.face);
    }
  }

  void _delete(BuildContext context, User user) async {
    int result = await databaseHelper.deleteUser(user.id);
    if (result != 0) {
      _showSnackBar(context, 'Participant Deleted Successfully');
      updateListView();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void navigateToDetail(User user, String title) async {
    bool result = await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return UserDetail(user, title);
    }));
    if (result == true) { updateListView();}
  }

  // void navigateToDetail(User user, String title) async {
  //   bool result = await Navigator.push(context, MaterialPageRoute(builder: (context) {
  //     return UserDetail(user, title);
  //   }));
  //   if (result == true) { updateListView();}
  // }

  // list users
  void updateListView() {
    final Future<Database> dbFuture = databaseHelper.initializeDatabase();
    dbFuture.then((database) {
      Future<List<User>> userListFuture = databaseHelper.getUserList();
      userListFuture.then((userList) {
        setState(() {
          this.userList = userList;
          this.count = userList.length;
        });
      });
    });
  }
  void show_modal(String name, String message) {
    AlertDialog alert_modal = AlertDialog(
      title: Text(name),
      content: Text(message),
    );
    showDialog(
      context: context,
      builder: (_) => alert_modal
    );
  }
} 
