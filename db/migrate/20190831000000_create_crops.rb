class CreateCrops < ActiveRecord::Migration[6.0]
  def change
    create_table :crops do |t|
      t.references :country, index: true, foreign_key: {on_delete: :nullify}
      t.references :province, index: true, foreign_key: {on_delete: :nullify}
      t.references :category, index: true, foreign_key: {on_delete: :nullify}
      t.string :shortname
      t.string :name
      t.string :photo
      t.float :water_min
      t.float :water_max
      t.float :nutrients
      t.float :temperature_min
      t.float :temperature_max
      t.float :soil
      t.float :elevation
      t.float :humidity_min
      t.float :humidity_max
      t.float :ph_min
      t.float :ph_max
      t.timestamps
    end
  end
end
